## Install vim

```
sudo apt-get install vim
```

## Install Vundle

```
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
```

## ColorScheme file

```
mkdir ~/.vim/colors
cp ~/vim-config/Tomorrow-Night.vim ~/.vim/colors/Tomorrow-Night.vim
```

## Install Plugins

In Vim enter command
```
:PluginUpdate
```
