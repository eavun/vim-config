set nocompatible
filetype plugin on
syntax on
colorscheme Tomorrow-Night
set number

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'StanAngeloff/php.vim'
Plugin 'chriskempson/tomorrow-theme'
Plugin 'scrooloose/nerdtree'
Plugin 'comments.vim'

call vundle#end()            " required

filetype plugin indent on    " required

autocmd vimenter * NERDTree

let NERDTreeShowHidden=1
